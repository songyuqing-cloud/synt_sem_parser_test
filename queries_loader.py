import os
from makeRules import MakeRules


def get_path(filename):
    # current_file = os.path.abspath(os.path.dirname(__file__))
    current_file =  os.path.join(os.path.dirname(__file__), "queries_samples", filename)
    # current_file = os.path.join(os.path.dirname(__file__), filename)
    return current_file


def queries_loader(file):
    absolute_path = get_path(file)
    with open(absolute_path) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    return content

def queries_maker(file):
    q = queries_loader(file)
    makeQueries = [MakeRules(x) for x in q]
    return makeQueries


