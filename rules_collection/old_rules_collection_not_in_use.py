from rule import Rule


def sems_0(sems):
    return sems[0]

def sems_1(sems):
    return sems[1]

def merge_dicts(d1, d2):
    if not d2:
        return d1
    result = d1.copy()
    result.update(d2)
    return result

rules_ot = [
    Rule('$ROOT', '$OTQuery', sems_0),
    Rule('$OTQuery', '$OTQueryElements',
         lambda sems: merge_dicts({'domain': 'ot'}, sems[0])),
    Rule('$OTQueryElements', '$OTQueryElement ?$OTQueryElements',
         lambda sems: merge_dicts(sems[0], sems[1])),
    Rule('$OTQueryElement', '$StringArgument', sems_0),
    Rule('$OTQueryElement', '$TabelArgument', sems_0),
]

###################################################################################################

lexical_rules = [

    Rule("$Maximum", "maximum", "maximum"),
    Rule("$Replace", "replace", "replace"),  # replace(s: string; sub, by: char): string {...}
    Rule("$ReplaceWord", "replace", "replaceWord"),  # replaceWord(s, sub: string; by = ""): string {...}

    Rule("$Trim", "trim", "trim"),
    Rule("$Trim", "trim", "trim"),

    Rule("$Write", "write", "write"),

    Rule("$Change", "change", "change"),

    Rule("$String", "string", "string"),
    Rule("$String", "String", "string"),
    Rule("$Lower", "lower", "lower"),
    Rule("$Lower", "Lower", "lower"),
    Rule("$Instead", "instead", "instead"),
    Rule("$For", "for", "for"),
    Rule("$Lower", "Lower", "lower"),
    Rule("$Make", "make", "make"),

    Rule("$Character", "A", "a"),  # replace this rules by annotator later
    Rule("$Character", "a", "a"),
    Rule("$Character", "b", "b"),
    Rule("$Character", "B", "b"),  # replace this rules by annotator later

    Rule("$Comma", ",", ","),
    Rule("$Space", " ", ","),

    Rule("$HashKey", "#", "#"),
    Rule("$Number", "1", lambda sems: (sems[0])),
    Rule("$Number", "2", lambda sems: (sems[0])),

    Rule("$Column", "column", sems_0)
]

##############################################################################################

unary_rules = [
    Rule("$Maximum", "$Biggest", "maximum"),
    Rule("$Delimiter", "$Comma", ","),
    Rule("$Delimiter", "$Semicolon", ";"),
    Rule("$Delimiter", "$Space", " "),
    Rule("$Delimiter", "$HashKey", "#"),
    Rule("$Command", "$Trim", "trim"),
    Rule("$Command", "$Change", "change"),
    Rule("$Parameter", "$String", "String"),
    Rule("$Argument", "$Number", "value"),
    Rule("$Argument", "$Token", sems_0),
    Rule("$Argument", "$Character", sems_0),
    Rule("$Change", "$Make", sems_0),

    Rule("$CompareRel", "$Maximum", sems_0),
    # Rule("$OperatorWithArgument", "$CompareRel", sems_0),

    # -- Adjectives --
    Rule("$Adjective", "$Lower", "ToLower"),
]

###############################################################################################

operator_rules = [
    Rule("$CompareRel", "$Maximum $Argument", lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$OperatorWithArgument', "$Command ?$Parameter", lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$OperatorWithArgument', "$Replace $Character", lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$SplitOperationWithDelimiter', '$Split $Delimiter', lambda sems: (sems[0], sems[1])),
    Rule('$SplitOperationWithDelimiterInBrackets', '$Split $Delimiter', lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$SplitOperationWithDelimiterInBrackets', '$Split $HashKey', lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$SplitOperationWithDelimiterInBrackets', '$Split $Comma', lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$OperatorWithTwoArguments', "$ReplaceCharacter $Character $Character",
         lambda sems: (sems[0], "(", sems[1], ",", sems[2], ")")),


    Rule('$OperatorWithArgument', "$Replace $Character $Character",
         lambda sems: (sems[0], "(", sems[1], ",", sems[2], ")")),


    Rule('$OperatorWithTwoArguments', "$Replace $Argument $Argument",
         lambda sems: (sems[0], "(", sems[1], ",", sems[2], ")")),


    Rule('$OperatorWithTwoArguments', "$Command $Parameter $Adjective",
         lambda sems: (sems[0], "(", sems[1], ",", sems[2], ")")),
    Rule('$OperatorWithTwoArguments', "$Command $Parameter $Adjective",
         lambda sems: (sems[1], ".", sems[2])),
]

###############################################################################################

rules_optionals = [
    Rule('$OTQueryElement', '$OTQueryElement $Optionals', sems_0),
    Rule('$OTQueryElement', '$Optionals $OTQueryElement', sems_1),

    Rule('$Optionals', '$Optional ?$Optionals'),

    Rule('$Optional', '$Stopword'),
    Rule('$Optional', '$Determiner'),

    Rule('$Stopword', 'all'),
    Rule('$Stopword', 'of'),
    Rule('$Stopword', 'what'),
    Rule('$Stopword', 'will'),
    Rule('$Stopword', 'it'),
    Rule('$Stopword', 'to'),

    Rule('$Determiner', 'a'),
    Rule('$Determiner', 'an'),
    Rule('$Determiner', 'the'),
]
def old_rules():
    return rules_ot + lexical_rules + unary_rules + operator_rules + rules_optionals

