from grammar import Grammar
from makeRules import MakeRules
from ot_parser import show_parse_trees

from queries_loader import queries_maker
from rules_collection.accumulatorForRules import collect_all_rules_from_csv_files
from rules_collection.old_rules_collection_not_in_use import old_rules

tb1_grammar = Grammar(collect_all_rules_from_csv_files())

queries_diesl = (queries_maker("queries_diesl"))


show_parse_trees(queries_diesl, tb1_grammar)


# q = MakeRules("maximum of column1"),