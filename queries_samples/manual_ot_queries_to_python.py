"""Module containing handcrafted ot queries woth python semantics for testing/training."""

import random
import makeRules
from collections import namedtuple


QtQuery = namedtuple("QtQuery", ["intent", "snippet"])

random.seed(3042330287)  # use seed for reproducable results

manualQtQueries = [
    #
    QtQuery("read Excel", "read_excel()"), # wich would be pandas again
    QtQuery("read Excel", "wb = xlrd.open_workbook(loc)"),  # wich need import xlrd
    QtQuery("write to csv", "writer = csv.writer(f)"),  # which needs import of csv
    QtQuery("write to csv", "f.to_csv()"),  # which needs import of pandas
    QtQuery("save as csv", "f.to_csv()"),  # which needs import of pandas

]
#
# random.shuffle(QtQuery)
#
# trainingManualPandasQueries = \
#     QtQuery[int(len(QtQuery) * 0.1):]
#
# testingManualPandasQueries = \
#     QtQuery[:int(len(QtQuery) * 0.1)]
