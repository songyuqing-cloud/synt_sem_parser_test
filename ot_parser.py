from collections import defaultdict
from itertools import product
from makeRules import MakeRules
from types import FunctionType
from collections.abc import Iterable

from grammar import is_lexical
from rule import Rule

MAX_CELL_CAPACITY = 10000
STOPPWORDS = ['in', 'to', 'with', 'of', 'for']


# Important for catching e.g. unary cycles.
def check_capacity(chart, i, j):
    if len(chart[(i, j)]) >= MAX_CELL_CAPACITY:
        print('Cell (%d, %d) has reached capacity %d' % (
            i, j, MAX_CELL_CAPACITY))
        return False
    return True


def apply_lexical_rules(grammar, chart, tokens, i, j):
    """Add parses to span (i, j) in chart by applying lexical rules from grammar to tokens."""
    for rule in grammar.lexical_rules[tuple(tokens[i:j])]:
        chart[(i, j)].append(Parse(rule, tokens[i:j]))


def apply_unary_rules(grammar, chart, i, j):
    """Add parses to chart cell (i, j) by applying unary rules."""
    # Note that the last line of this method can add new parses to chart[(i,
    # j)], the list over which we are iterating.  Because of this, we
    # essentially get unary closure "for free".  (However, if the grammar
    # contains unary cycles, we'll get stuck in a loop, which is one reason for
    # check_capacity().)
    if hasattr(grammar, 'unary_rules'):
        for parse in chart[(i, j)]:
            for rule in grammar.unary_rules[(parse.rule.lhs,)]:
                if not check_capacity(chart, i, j):
                    return
                chart[(i, j)].append(Parse(rule, [parse]))


def apply_binary_rules(grammar, chart, i, j):
    """Add parses to span (i, j) in chart by applying binary rules from grammar."""
    for k in range(i + 1, j):  # all ways of splitting the span into two subspans
        for parse_1, parse_2 in product(chart[(i, k)], chart[(k, j)]):
            for rule in grammar.binary_rules[(parse_1.rule.lhs, parse_2.rule.lhs)]:
                chart[(i, j)].append(Parse(rule, [parse_1, parse_2]))


def apply_annotators(grammar, chart, tokens, i, j):
    """Add parses to chart cell (i, j) by applying annotators."""
    if hasattr(grammar, 'annotators'):
        for annotator in grammar.annotators:
            for category, semantics in annotator.annotate(tokens[i:j]):
                if not check_capacity(chart, i, j):
                    return
                rule = Rule(category, tuple(tokens[i:j]), semantics)
                chart[(i, j)].append(Parse(rule, tokens[i:j]))


def compute_semantics(parse):
    if is_lexical(parse.rule) or not isinstance(parse.rule.sem, FunctionType):
        return parse.rule.sem
    else:
        return parse.rule.sem([child.semantics for child in parse.children])


def filterStopwords(eingabeText):
    out = [word for word in eingabeText if word not in STOPPWORDS]
    return out


def parse_input(grammar, input):
    """Returns a list of all parses for input using grammar."""
    tokens = input.split()
    tokens = filterStopwords(tokens)
    chart = defaultdict(list)
    for j in range(1, len(tokens) + 1):
        for i in range(j - 1, -1, -1):
            apply_annotators(grammar, chart, tokens, i, j)
            apply_lexical_rules(grammar, chart, tokens, i, j)
            apply_binary_rules(grammar, chart, i, j)
            apply_unary_rules(grammar, chart, i, j)
    parses = chart[(0, len(tokens))]
    if hasattr(grammar, 'start_symbol') and grammar.start_symbol:
        parses = [parse for parse in parses if parse.rule.lhs == grammar.start_symbol]
    return parses


def show_parse_trees(tb1_list, grammar):
    for example in tb1_list:
        parses = parse_input(grammar, example.input)
        print()
        print('%-16s %s' % ('input', example.input))
        if not parses:
            print(f"---no parses were found for this input: {example.input}")
        for idx, parse in enumerate(parses):
            print('%-16s %s' % ('parse %d' % idx, parse))
            print("--------parse ----")
            print(parse)
            print("---------semantic-----")
            print(parse.semantics)
            print("------Output----------")
            t = parse.semantics

            if isinstance(t, Iterable):
                text = "".join(t)
                print(text)
                print("\n")
                print("\n")
            else:
                print(t)


def show_syntactic_tree(grammar, example):
    """Show all the syntactic trees for an input """

    parses = parse_input(grammar, example)
    print(f"input: {example}")
    for index, parse in enumerate(parses):
        print(f"parse number {index}: {parse}")


class Parse:
    def __init__(self, rule, children):
        self.rule = rule
        self.children = tuple(children[:])
        self.semantics = compute_semantics(self)
        self.score = float('NaN')  # not now
        self.denotation = None  # not now

    def __str__(self):
        return '(%s %s)' % (self.rule.lhs, ' '.join([str(c) for c in self.children]))
